arr = [1,2,6,8,2,-6,3,4]

def sorting(arrx):
    # Проверка на количество элементов в списке
    # Список из 1 || 0 элементов уже сортирован
    if len(arrx)<= 1:
        return arrx
    elem = arrx[0]
    left = list(filter(lambda x: x < elem, arrx)) # Элементы меньше опорного
    center = [i for i in arrx if i == elem] # Выбор опроного элемента
    right = list(filter(lambda x: x > elem, arrx)) # Элементы больше опорного

    return sorting(left) + center + sorting(right) # Возврат сортированного списка

print(sorting(arr))

